Main module
===========
Main module for yii2-advanced projects templates

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist vilderr/yii2-module-main "*"
```

or add

```
"vilderr/yii2-module-main": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \vilderr\modules\main\AutoloadExample::widget(); ?>```